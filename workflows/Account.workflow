<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>TAFS_Update</fullName>
        <field>TAFS_Client_Status__c</field>
        <literalValue>RS Assignment</literalValue>
        <name>TAFS Update Client Status To RS Assignme</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>TAFS First Funding Date Created Updated</fullName>
        <actions>
            <name>TAFS_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>NOT(ISBLANK(TAFS_First_Funding_Date__c))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
