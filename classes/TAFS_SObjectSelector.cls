/****************************************************************************************
* Created By      :  Pankaj Singh
* Create Date     :  24/03/2016
* Description     :  Selector class which contains selector methods for all the objects
* Modification Log:  Initial version.
***************************************************************************************/
public with sharing class TAFS_SObjectSelector{
    /************************************************************************************
    * Method       :    selectEventByIds
    * Description  :    returns a list of Events based on the event Ids passed
    * Parameter    :    Set<string>   
    * Return Type  :    List<Event>
    *************************************************************************************/
    public List<Event> selectEventByIds(Set<string> eventIds)
    {
        return [SELECT Id ,Subject,Type,StartDateTime ,TAFS_Additional_Client_Welcome_Contact__r.Name,TAFS_Onboarding_Specialist__r.Name, what.Name, Who.Name FROM Event where Id IN : eventIds];
    }
    
    public List<TAFS_Welcome_Client__c> selectWelcomeClientByIds(Set<string> clientIds)
    {
        return [SELECT Id,TAFS_Client_Name__c,TAFS_Status__c,TAFS_Activate_ATM_option__c,TAFS_Activate_EFS_cards__c,TAFS_Activity_Id__c,
                TAFS_Additional_Client_Welcome_Contact__c,TAFS_Called_No_Contact_Counter__c, TAFS_Client_Welcome_Contact__c,TAFS_Driver_Version__c,
                TAFS_Conference_EFS_Account_Management__c, TAFS_Confirm_client_can_log_into_Sight__c, TAFS_Download_Mobile_app__c,
                TAFS_EFS_Activation_Setup_Completed__c,TAFS_Emergency_Roadside_Assistance__c, TAFS_Give_6_digit_account_number__c,TAFS_Notes__c,
                TAFS_Gold_Tire_Discount_Program__c, TAFS_Go_through_Invoice_Search__c,TAFS_submit_credit_check_on_new_debtor__c,
                TAFS_Onboarding_Specialist__c,TAFS_Owner_Version__c, TAFS_Paper_Submission_Review_Completed__c, TAFS_Refer_a_Friend__c,
                TAFS_Portal_Review_Completed__c, TAFS_Review_detail_tab__c,TAFS_Review_look_up_for__c,TAFS_Review_main_page__c,
                TAFS_Review_procedures__c, TAFS_Review_reports__c, TAFS_Review_required_paper_work__c, TAFS_Review_requirements_paper_work__c,
                TAFS_Review_transaction_limit_defaults__c, TAFS_Review_transaction_tab__c, TAFS_Sales_Account_Manager__c, TAFS_Set_up_ACH_option__c,
                TAFS_Sales_Opportunities_Review_Complete__c, TAFS_Set_up_additional_functionality__c,TAFS_Show_fast_aging_with_balances__c,
                TAFS_Show_reporting__c, TAFS_Fuel_Rebate__c, TAFS_Validate_EFS_phone_no_with_client__c, TAFS_Walk_through_EFS_money_codes__c,
                TAFS_Scheduled_DateTime__c,TAFS_Conf_to_EFS_Acc_Management_DT__c,TAFS_Confirm_client_log_into_Sight_DT__c,TAFS_Download_Mobile_app_DT__c,TAFS_Driver_Version_DT__c,
                TAFS_EFS_Activation_SetUp_Completed_DT__c,TAFS_Emergency_Roadside_Assistance_DT__c,TAFS_Give_6_digit_account_number_DT__c,TAFS_Gold_Tire_Discount_Program_DT__c,
                TAFS_Go_through_Invoice_Search_DT__c,TAFS_submit_credit_check_debtor_DT__c,TAFS_Owner_Versioin_DT__c,TAFS_Paperwork_Sub_Review_Completed_DT__c ,
                TAFS_Portal_Review_Completed_DT__c,TAFS_Refer_a_Friend_DT__c,TAFS_Review_detail_tab_DT__c,TAFS_Review_look_up_for_DT__c,TAFS_Review_main_page_DT__c,
                TAFS_Review_procedures_DT__c,TAFS_Review_reports_DT__c,TAFS_Review_reqs_of_legible_paperwork_DT__c,TAFS_Review_required_paper_work_DT__c,
                TAFS_Review_transac_limit_defaults_DT__c,TAFS_Review_transaction_tab_DT__c,TAFS_Sales_Opp_Review_Completed_DT__c,TAFS_Set_up_ACH_option_DT__c,
                TAFS_Set_up_additional_functionality_DT__c,TAFS_Show_fast_aging_balances_DT__c,TAFS_Show_reporting_DT__c,TAFS_Fuel_Rebate_DT__c,TAFS_Validate_EFS_phone_with_client_DT__c,
                TAFS_Walk_through_EFS_codes_DT__c FROM TAFS_Welcome_Client__c where TAFS_Client_Name__c IN : clientIds];
    }
    
    /************************************************************************************
    * Method       :    selectTeamMemberByAccountId
    * Description  :    returns a list of Account Team Members based on the ACcount Id and team role passed
    * Parameter    :    Id , String   
    * Return Type  :    List<AccountTeamMember>
    *************************************************************************************/
    public List<AccountTeamMember> selectTeamMemberByAccountId(Id accountId, String teamRole)
    {            
        return [SELECT Id ,AccountId,TeamMemberRole, UserId FROM AccountTeamMember where AccountId =: accountId AND TeamMemberRole IN :teamRole.split(';')];
    }
    
    /************************************************************************************
    * Method       :    selectByAccountId
    * Description  :    returns a list of Account based on the set of Ids passed
    * Parameter    :    set<String>  
    * Return Type  :    List<AccountTeamMember>
    *************************************************************************************/
    public List<Account> selectByAccountId(set<String> accountIds)
    {            
        return [SELECT Id ,TAFS_Client_Welcome_Complete__c,TAFS_Client_Status__c, TAFS_Last_Successful_Check_In_Date__c FROM Account where Id IN : accountIds];
    }
}