/****************************************************************************************
* Created By      :  Pankaj Singh
* Create Date     :  24/03/2016
* Description     :  Service class for the Client welcome/training page
* Modification Log:  Initial version.
***************************************************************************************/
public with sharing class TAFS_EventsService{
       
    public class EventsServiceException extends Exception {}
    
    public class EventStub
    {
        public TAFS_Welcome_Client__c welcomeClientObj {get;set;}
        public Id parentid {get;set;}                    //this can be AccountId or any other parent of welcome client
        public Event activity {get;set;}                  //this is the event id for which the welcome client record is being created
        public DateTime callStartDateTime{get;set;}      //this the start time for the welcome client call
        
        public EventStub(TAFS_Welcome_Client__c welcomeClientObj, Id parentId, Event activity, DateTime callStartDateTime){
            
            this.welcomeClientObj = welcomeClientObj;
            this.parentId = parentId;
            this.activity = activity;
            this.callStartDateTime = callStartDateTime;
        }
    }
    /************************************************************************************
    * Method       :    createwelcomeClientRecords
    * Description  :    Method to create or update the welcome client records.
    * Parameter    :    List<EventStub>    
    * Return Type  :    void
    *************************************************************************************/
    public static void createwelcomeClientRecords(List<EventStub> eventStubList){
    
        List<TAFS_Welcome_Client__c> welcomeClientList =  new List<TAFS_Welcome_Client__c>();
        List<Account> clientList =  new List<Account>();
        set<string> accidList = new set<string>();
        
        if(eventStubList == null && eventStubList.size() <=0){
            //Very bad data
            throw new EventsServiceException('No work to do');
            
        }            
        for(EventStub eventstub : eventStubList){
            
            if(eventstub.activity.Type == 'Client Welcome'){              
               
               if(eventstub.welcomeClientObj.TAFS_Conference_EFS_Account_Management__c && eventstub.welcomeClientObj.TAFS_Confirm_client_can_log_into_Sight__c 
               && eventstub.welcomeClientObj.TAFS_Download_Mobile_app__c && eventstub.welcomeClientObj.TAFS_Driver_Version__c  
               && eventstub.welcomeClientObj.TAFS_Emergency_Roadside_Assistance__c && eventstub.welcomeClientObj.TAFS_Give_6_digit_account_number__c
               && eventstub.welcomeClientObj.TAFS_Gold_Tire_Discount_Program__c && eventstub.welcomeClientObj.TAFS_Go_through_Invoice_Search__c && eventstub.welcomeClientObj.TAFS_submit_credit_check_on_new_debtor__c  
               && eventstub.welcomeClientObj.TAFS_Owner_Version__c && eventstub.welcomeClientObj.TAFS_Refer_a_Friend__c 
               && eventstub.welcomeClientObj.TAFS_Review_detail_tab__c && eventstub.welcomeClientObj.TAFS_Review_look_up_for__c  
               && eventstub.welcomeClientObj.TAFS_Review_main_page__c && eventstub.welcomeClientObj.TAFS_Review_procedures__c && eventstub.welcomeClientObj.TAFS_Review_reports__c 
               && eventstub.welcomeClientObj.TAFS_Review_requirements_paper_work__c && eventstub.welcomeClientObj.TAFS_Review_required_paper_work__c && eventstub.welcomeClientObj.TAFS_Review_transaction_limit_defaults__c 
               && eventstub.welcomeClientObj.TAFS_Review_transaction_tab__c && eventstub.welcomeClientObj.TAFS_Set_up_ACH_option__c 
               && eventstub.welcomeClientObj.TAFS_Set_up_additional_functionality__c && eventstub.welcomeClientObj.TAFS_Show_fast_aging_with_balances__c && eventstub.welcomeClientObj.TAFS_Show_reporting__c 
               && eventstub.welcomeClientObj.TAFS_Fuel_Rebate__c && eventstub.welcomeClientObj.TAFS_Validate_EFS_phone_no_with_client__c && eventstub.welcomeClientObj.TAFS_Walk_through_EFS_money_codes__c){
                   
                   accidList.add(eventstub.activity.whatId);
                   eventstub.welcomeClientObj.TAFS_Status__c = 'Closed';
               }
            } 
           
            if(eventstub.welcomeClientObj.TAFS_Driver_Version__c && eventstub.welcomeClientObj.TAFS_Owner_Version__c && eventstub.welcomeClientObj.TAFS_Review_requirements_paper_work__c 
               && eventstub.welcomeClientObj.TAFS_Review_required_paper_work__c && eventstub.welcomeClientObj.TAFS_Review_procedures__c){
                
                   eventstub.welcomeClientObj.TAFS_Paper_Submission_Review_Completed__c = true;
            }
            else{
                   eventstub.welcomeClientObj.TAFS_Paper_Submission_Review_Completed__c = false;
            }
            if(eventstub.welcomeClientObj.TAFS_Confirm_client_can_log_into_Sight__c && eventstub.welcomeClientObj.TAFS_Review_main_page__c && eventstub.welcomeClientObj.TAFS_Review_reports__c
               && eventstub.welcomeClientObj.TAFS_Show_fast_aging_with_balances__c && eventstub.welcomeClientObj.TAFS_Go_through_Invoice_Search__c && eventstub.welcomeClientObj.TAFS_Review_transaction_tab__c 
               && eventstub.welcomeClientObj.TAFS_Review_look_up_for__c && eventstub.welcomeClientObj.TAFS_submit_credit_check_on_new_debtor__c && eventstub.welcomeClientObj.TAFS_Review_detail_tab__c){
               
                eventstub.welcomeClientObj.TAFS_Portal_Review_Completed__c = true;
            }
             else{
                 eventstub.welcomeClientObj.TAFS_Portal_Review_Completed__c = false;
            }  
            if(eventstub.welcomeClientObj.TAFS_Conference_EFS_Account_Management__c && eventstub.welcomeClientObj.TAFS_Give_6_digit_account_number__c && eventstub.welcomeClientObj.TAFS_Set_up_ACH_option__c
               && eventstub.welcomeClientObj.TAFS_Activate_ATM_option__c && eventstub.welcomeClientObj.TAFS_Activate_EFS_cards__c && eventstub.welcomeClientObj.TAFS_Walk_through_EFS_money_codes__c 
               && eventstub.welcomeClientObj.TAFS_Review_transaction_limit_defaults__c && eventstub.welcomeClientObj.TAFS_Set_up_additional_functionality__c && eventstub.welcomeClientObj.TAFS_Show_reporting__c 
               && eventstub.welcomeClientObj.TAFS_Download_Mobile_app__c && eventstub.welcomeClientObj.TAFS_Validate_EFS_phone_no_with_client__c){
                
                eventstub.welcomeClientObj.TAFS_EFS_Activation_Setup_Completed__c = true;
            }
            else{
                eventstub.welcomeClientObj.TAFS_EFS_Activation_Setup_Completed__c = false;
            }  
            if(eventstub.welcomeClientObj.TAFS_Refer_a_Friend__c && eventstub.welcomeClientObj.TAFS_Gold_Tire_Discount_Program__c && eventstub.welcomeClientObj.TAFS_Emergency_Roadside_Assistance__c
               && eventstub.welcomeClientObj.TAFS_Fuel_Rebate__c){
                eventstub.welcomeClientObj.TAFS_Sales_Opportunities_Review_Complete__c = true;
            } 
            else{
                eventstub.welcomeClientObj.TAFS_Sales_Opportunities_Review_Complete__c = false;
            }                               
            if(eventstub.parentId.getSObjectType() == Account.sObjectType){
            
                eventstub.welcomeClientObj.TAFS_Client_Name__c = eventstub.parentId;
            }                
            eventstub.welcomeClientObj.TAFS_Activity_Id__c = eventstub.activity.Id;
          
            //stamping the DateTime when the call was started
            if(eventstub.welcomeClientObj.TAFS_Conference_EFS_Account_Management__c && eventstub.welcomeClientObj.TAFS_Conf_to_EFS_Acc_Management_DT__c==null)
            eventstub.welcomeClientObj.TAFS_Conf_to_EFS_Acc_Management_DT__c = eventstub.callStartDateTime;
            
            else if(!eventstub.welcomeClientObj.TAFS_Conference_EFS_Account_Management__c)
            eventstub.welcomeClientObj.TAFS_Conf_to_EFS_Acc_Management_DT__c = null;
            
            if(eventstub.welcomeClientObj.TAFS_Confirm_client_can_log_into_Sight__c && eventstub.welcomeClientObj.TAFS_Confirm_client_log_into_Sight_DT__c==null)
            eventstub.welcomeClientObj.TAFS_Confirm_client_log_into_Sight_DT__c = eventstub.callStartDateTime;
            
            else if(!eventstub.welcomeClientObj.TAFS_Confirm_client_can_log_into_Sight__c )
            eventstub.welcomeClientObj.TAFS_Confirm_client_log_into_Sight_DT__c = null;
            
            if(eventstub.welcomeClientObj.TAFS_Download_Mobile_app__c && eventstub.welcomeClientObj.TAFS_Download_Mobile_app_DT__c==null)
            eventstub.welcomeClientObj.TAFS_Download_Mobile_app_DT__c = eventstub.callStartDateTime;
            
            else if(!eventstub.welcomeClientObj.TAFS_Download_Mobile_app__c )
            eventstub.welcomeClientObj.TAFS_Download_Mobile_app_DT__c = null;
            
            if(eventstub.welcomeClientObj.TAFS_Driver_Version__c && eventstub.welcomeClientObj.TAFS_Driver_Version_DT__c==null)
            eventstub.welcomeClientObj.TAFS_Driver_Version_DT__c = eventstub.callStartDateTime;
            
            else if(!eventstub.welcomeClientObj.TAFS_Driver_Version__c)
            eventstub.welcomeClientObj.TAFS_Driver_Version_DT__c= null;
            
            if(eventstub.welcomeClientObj.TAFS_EFS_Activation_Setup_Completed__c && eventstub.welcomeClientObj.TAFS_EFS_Activation_SetUp_Completed_DT__c==null)
            eventstub.welcomeClientObj.TAFS_EFS_Activation_SetUp_Completed_DT__c = eventstub.callStartDateTime;
            
            else if(!eventstub.welcomeClientObj.TAFS_EFS_Activation_Setup_Completed__c )
            eventstub.welcomeClientObj.TAFS_EFS_Activation_SetUp_Completed_DT__c= null;
            
            if(eventstub.welcomeClientObj.TAFS_Emergency_Roadside_Assistance__c && eventstub.welcomeClientObj.TAFS_Emergency_Roadside_Assistance_DT__c==null)
            eventstub.welcomeClientObj.TAFS_Emergency_Roadside_Assistance_DT__c = eventstub.callStartDateTime;
            
            else if(!eventstub.welcomeClientObj.TAFS_Emergency_Roadside_Assistance__c )
            eventstub.welcomeClientObj.TAFS_Emergency_Roadside_Assistance_DT__c= null;
            
            if(eventstub.welcomeClientObj.TAFS_Give_6_digit_account_number__c && eventstub.welcomeClientObj.TAFS_Give_6_digit_account_number_DT__c==null)
            eventstub.welcomeClientObj.TAFS_Give_6_digit_account_number_DT__c = eventstub.callStartDateTime;
            
            else if(!eventstub.welcomeClientObj.TAFS_Give_6_digit_account_number__c )
            eventstub.welcomeClientObj.TAFS_Give_6_digit_account_number_DT__c= null;
            
            if(eventstub.welcomeClientObj.TAFS_Gold_Tire_Discount_Program__c && eventstub.welcomeClientObj.TAFS_Gold_Tire_Discount_Program_DT__c==null)
            eventstub.welcomeClientObj.TAFS_Gold_Tire_Discount_Program_DT__c = eventstub.callStartDateTime;
            
            else if(!eventstub.welcomeClientObj.TAFS_Gold_Tire_Discount_Program__c )
            eventstub.welcomeClientObj.TAFS_Gold_Tire_Discount_Program_DT__c= null;
            
            if(eventstub.welcomeClientObj.TAFS_Go_through_Invoice_Search__c && eventstub.welcomeClientObj.TAFS_Go_through_Invoice_Search_DT__c==null)
            eventstub.welcomeClientObj.TAFS_Go_through_Invoice_Search_DT__c = eventstub.callStartDateTime;
            
            else if(!eventstub.welcomeClientObj.TAFS_Go_through_Invoice_Search__c )
            eventstub.welcomeClientObj.TAFS_Go_through_Invoice_Search_DT__c= null;
            
            if(eventstub.welcomeClientObj.TAFS_submit_credit_check_on_new_debtor__c && eventstub.welcomeClientObj.TAFS_submit_credit_check_debtor_DT__c==null)
            eventstub.welcomeClientObj.TAFS_submit_credit_check_debtor_DT__c = eventstub.callStartDateTime;
            
            else if(!eventstub.welcomeClientObj.TAFS_submit_credit_check_on_new_debtor__c)
            eventstub.welcomeClientObj.TAFS_submit_credit_check_debtor_DT__c= null;
            
            if(eventstub.welcomeClientObj.TAFS_Owner_Version__c && eventstub.welcomeClientObj.TAFS_Owner_Versioin_DT__c==null)            
            eventstub.welcomeClientObj.TAFS_Owner_Versioin_DT__c = eventstub.callStartDateTime;
            
            else if(!eventstub.welcomeClientObj.TAFS_Owner_Version__c )
            eventstub.welcomeClientObj.TAFS_Owner_Versioin_DT__c= null;
            
            if(eventstub.welcomeClientObj.TAFS_Paper_Submission_Review_Completed__c && eventstub.welcomeClientObj.TAFS_Paperwork_Sub_Review_Completed_DT__c ==null)
            eventstub.welcomeClientObj.TAFS_Paperwork_Sub_Review_Completed_DT__c = eventstub.callStartDateTime;
            
            else if(!eventstub.welcomeClientObj.TAFS_Paper_Submission_Review_Completed__c)
            eventstub.welcomeClientObj.TAFS_Paperwork_Sub_Review_Completed_DT__c = null;            
            
            if(eventstub.welcomeClientObj.TAFS_Portal_Review_Completed__c && eventstub.welcomeClientObj.TAFS_Portal_Review_Completed_DT__c==null)
            eventstub.welcomeClientObj.TAFS_Portal_Review_Completed_DT__c = eventstub.callStartDateTime;
            
            else if(!eventstub.welcomeClientObj.TAFS_Portal_Review_Completed__c )
            eventstub.welcomeClientObj.TAFS_Portal_Review_Completed_DT__c = null; 
            
            if(eventstub.welcomeClientObj.TAFS_Refer_a_Friend__c && eventstub.welcomeClientObj.TAFS_Refer_a_Friend_DT__c==null)
            eventstub.welcomeClientObj.TAFS_Refer_a_Friend_DT__c = eventstub.callStartDateTime;
            
            else if(!eventstub.welcomeClientObj.TAFS_Refer_a_Friend__c )
            eventstub.welcomeClientObj.TAFS_Refer_a_Friend_DT__c= null; 
            
            if(eventstub.welcomeClientObj.TAFS_Review_detail_tab__c && eventstub.welcomeClientObj.TAFS_Review_detail_tab_DT__c==null)
            eventstub.welcomeClientObj.TAFS_Review_detail_tab_DT__c = eventstub.callStartDateTime;
            
            else if(!eventstub.welcomeClientObj.TAFS_Review_detail_tab__c )
            eventstub.welcomeClientObj.TAFS_Review_detail_tab_DT__c= null;
            
            if(eventstub.welcomeClientObj.TAFS_Review_look_up_for__c && eventstub.welcomeClientObj.TAFS_Review_look_up_for_DT__c==null)
            eventstub.welcomeClientObj.TAFS_Review_look_up_for_DT__c = eventstub.callStartDateTime;
            
            else if(!eventstub.welcomeClientObj.TAFS_Review_look_up_for__c)
            eventstub.welcomeClientObj.TAFS_Review_look_up_for_DT__c = null;
            
            if(eventstub.welcomeClientObj.TAFS_Review_main_page__c && eventstub.welcomeClientObj.TAFS_Review_main_page_DT__c==null)
            eventstub.welcomeClientObj.TAFS_Review_main_page_DT__c = eventstub.callStartDateTime;
            
            else if(!eventstub.welcomeClientObj.TAFS_Review_main_page__c )
            eventstub.welcomeClientObj.TAFS_Review_main_page_DT__c = null;
            
            if(eventstub.welcomeClientObj.TAFS_Review_procedures__c &&  eventstub.welcomeClientObj.TAFS_Review_procedures_DT__c==null)
            eventstub.welcomeClientObj.TAFS_Review_procedures_DT__c = eventstub.callStartDateTime;
            
            else if(!eventstub.welcomeClientObj.TAFS_Review_procedures__c )
            eventstub.welcomeClientObj.TAFS_Review_procedures_DT__c = null;
            
            if(eventstub.welcomeClientObj.TAFS_Review_reports__c && eventstub.welcomeClientObj.TAFS_Review_reports_DT__c==null)
            eventstub.welcomeClientObj.TAFS_Review_reports_DT__c = eventstub.callStartDateTime;
            
            else if(!eventstub.welcomeClientObj.TAFS_Review_reports__c  )
            eventstub.welcomeClientObj.TAFS_Review_reports_DT__c= null;
            
            if(eventstub.welcomeClientObj.TAFS_Review_requirements_paper_work__c && eventstub.welcomeClientObj.TAFS_Review_reqs_of_legible_paperwork_DT__c==null)
            eventstub.welcomeClientObj.TAFS_Review_reqs_of_legible_paperwork_DT__c  = eventstub.callStartDateTime;
            
            else if(!eventstub.welcomeClientObj.TAFS_Review_requirements_paper_work__c )
            eventstub.welcomeClientObj.TAFS_Review_reqs_of_legible_paperwork_DT__c= null;
            
            if(eventstub.welcomeClientObj.TAFS_Review_required_paper_work__c && eventstub.welcomeClientObj.TAFS_Review_required_paper_work_DT__c==null)
            eventstub.welcomeClientObj.TAFS_Review_required_paper_work_DT__c = eventstub.callStartDateTime;
            
            else if(!eventstub.welcomeClientObj.TAFS_Review_required_paper_work__c )
            eventstub.welcomeClientObj.TAFS_Review_required_paper_work_DT__c = null;
            
            if(eventstub.welcomeClientObj.TAFS_Review_transaction_limit_defaults__c && eventstub.welcomeClientObj.TAFS_Review_transac_limit_defaults_DT__c==null)
            eventstub.welcomeClientObj.TAFS_Review_transac_limit_defaults_DT__c  = eventstub.callStartDateTime;
            
            else if(!eventstub.welcomeClientObj.TAFS_Review_transaction_limit_defaults__c )
            eventstub.welcomeClientObj.TAFS_Review_transac_limit_defaults_DT__c= null;
            
            if(eventstub.welcomeClientObj.TAFS_Review_transaction_tab__c && eventstub.welcomeClientObj.TAFS_Review_transaction_tab_DT__c==null)
            eventstub.welcomeClientObj.TAFS_Review_transaction_tab_DT__c = eventstub.callStartDateTime;
            
            else if(!eventstub.welcomeClientObj.TAFS_Review_transaction_tab__c )
            eventstub.welcomeClientObj.TAFS_Review_transaction_tab_DT__c = null;
            
            if(eventstub.welcomeClientObj.TAFS_Sales_Opportunities_Review_Complete__c &&  eventstub.welcomeClientObj.TAFS_Sales_Opp_Review_Completed_DT__c==null)
            eventstub.welcomeClientObj.TAFS_Sales_Opp_Review_Completed_DT__c  = eventstub.callStartDateTime;
            
            else if(!eventstub.welcomeClientObj.TAFS_Sales_Opportunities_Review_Complete__c)
            eventstub.welcomeClientObj.TAFS_Sales_Opp_Review_Completed_DT__c= null;
            
            if(eventstub.welcomeClientObj.TAFS_Set_up_ACH_option__c && eventstub.welcomeClientObj.TAFS_Set_up_ACH_option_DT__c==null)
            eventstub.welcomeClientObj.TAFS_Set_up_ACH_option_DT__c = eventstub.callStartDateTime;
            
            else if(!eventstub.welcomeClientObj.TAFS_Set_up_ACH_option__c)
            eventstub.welcomeClientObj.TAFS_Set_up_ACH_option_DT__c= null;
            
            if(eventstub.welcomeClientObj.TAFS_Set_up_additional_functionality__c && eventstub.welcomeClientObj.TAFS_Set_up_additional_functionality_DT__c==null)
            eventstub.welcomeClientObj.TAFS_Set_up_additional_functionality_DT__c = eventstub.callStartDateTime;
            
            else if(!eventstub.welcomeClientObj.TAFS_Set_up_additional_functionality__c )
            eventstub.welcomeClientObj.TAFS_Set_up_additional_functionality_DT__c= null;
            
            if(eventstub.welcomeClientObj.TAFS_Show_fast_aging_with_balances__c && eventstub.welcomeClientObj.TAFS_Show_fast_aging_balances_DT__c==null)
            eventstub.welcomeClientObj.TAFS_Show_fast_aging_balances_DT__c = eventstub.callStartDateTime;
            
            else if(!eventstub.welcomeClientObj.TAFS_Show_fast_aging_with_balances__c )
            eventstub.welcomeClientObj.TAFS_Show_fast_aging_balances_DT__c= null;
            
            if(eventstub.welcomeClientObj.TAFS_Show_reporting__c && eventstub.welcomeClientObj.TAFS_Show_reporting_DT__c==null)
            eventstub.welcomeClientObj.TAFS_Show_reporting_DT__c = eventstub.callStartDateTime;
            
            else if(!eventstub.welcomeClientObj.TAFS_Show_reporting__c )
            eventstub.welcomeClientObj.TAFS_Show_reporting_DT__c= null;
            
            if(eventstub.welcomeClientObj.TAFS_Fuel_Rebate__c && eventstub.welcomeClientObj.TAFS_Fuel_Rebate_DT__c==null)
            eventstub.welcomeClientObj.TAFS_Fuel_Rebate_DT__c = eventstub.callStartDateTime;
            
            else if(!eventstub.welcomeClientObj.TAFS_Fuel_Rebate__c )
            eventstub.welcomeClientObj.TAFS_Fuel_Rebate_DT__c= null;
            
            if(eventstub.welcomeClientObj.TAFS_Validate_EFS_phone_no_with_client__c && eventstub.welcomeClientObj.TAFS_Validate_EFS_phone_with_client_DT__c==null)
            eventstub.welcomeClientObj.TAFS_Validate_EFS_phone_with_client_DT__c = eventstub.callStartDateTime;
            
            else if(!eventstub.welcomeClientObj.TAFS_Validate_EFS_phone_no_with_client__c )
            eventstub.welcomeClientObj.TAFS_Validate_EFS_phone_with_client_DT__c= null;
            
            if(eventstub.welcomeClientObj.TAFS_Walk_through_EFS_money_codes__c && eventstub.welcomeClientObj.TAFS_Walk_through_EFS_codes_DT__c==null)
            eventstub.welcomeClientObj.TAFS_Walk_through_EFS_codes_DT__c = eventstub.callStartDateTime;
            
            else if(!eventstub.welcomeClientObj.TAFS_Walk_through_EFS_money_codes__c  )
            eventstub.welcomeClientObj.TAFS_Walk_through_EFS_codes_DT__c = null;
            
            welcomeClientList.add(eventstub.welcomeClientObj);
         } 
         if(!welcomeClientList.isEmpty()){
           
             try{
            
                upsert welcomeClientList;
             }
            catch(exception e)
            {
                 // very bad data!
                 throw new EventsServiceException('Invalid Data Supplied - Unable to insert welcome client record due to invalid data');
            }
        }
        if(!accidList.isEmpty()){
           
            for(Account acc : new TAFS_SObjectSelector().selectByAccountId(accidList)){
                acc.TAFS_Client_Welcome_Complete__c = true;
                acc.TAFS_Client_Status__c = 'Awaiting First Funding';
                acc.TAFS_Last_Successful_Check_In_Date__c = Date.Today();
                clientList.add(acc);
            }
        }
        if(!clientList.isEmpty()){
           
             try{
            
                update clientList;
             }
            catch(exception e)
            {
                 // very bad data!
                 throw new EventsServiceException('Invalid Data Supplied - Unable to insert client record due to invalid data');
            }
        }        
    }
}