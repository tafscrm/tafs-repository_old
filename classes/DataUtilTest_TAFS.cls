/****************************************************************************************
* Create By     :   Pankaj Singh
* Create Date   :   04/07/2016
* Description   :   Util class to setup test data for test class
* Modification Log:
*-----------------------------------------------------------------------------
** Developer                    Date          Description
** ----------------------------------------------------------------------------
** Pankaj Singh                 04/07/2016    Initial version.

*****************************************************************************************/
public with sharing class DataUtilTest_TAFS {
    
    
    /************************************************************************************
    * Method       :    createContacts
    * Description  :    create a list of contacts
    * Parameter    :    integer,Id    
    * Return Type  :    list<Contact>
    *************************************************************************************/
    public static list<Contact> createContacts(integer total, Id accountId) 
    {
        list<Contact> contacts = new list<Contact>();
        for(integer i = 0; i < total; i++)
        {
            contacts.add(new Contact(AccountId                      = accountId,
                                     LastName                       = 'test',
                                     MobilePhone                    = '9663066004',
                                     TAFS_Preferred_Language__c     = 'English',
                                     FirstName                      = 'test',
                                     Salutation                     = 'Mr',
                                     TAFS_Authority__c              = true));
                      
        }
        insert contacts;
        return contacts;
    }
    
    /************************************************************************************
    * Method       :    createEvents
    * Description  :    create a list of events
    * Parameter    :    integer,Id    
    * Return Type  :    list<Event>
    *************************************************************************************/
    public static list<Event> createEvents(integer total, Id contactId, Id accountId) 
    {
        list<Event> events = new list<Event>();
        for(integer i = 0; i < total; i++)
        {
            events.add(new Event(Subject              = 'Client Welcome',
                                 StartDateTime        = System.now(),
                                 EndDateTime          = System.now()+1,
                                 OwnerId              = UserInfo.getUserId(),
                                 WhoId                = contactId,
                                 WhatId               = accountId,
                                 Type                 = 'Client Welcome'
                               ));
        }
        insert events;
        return events;
    }
    
    /************************************************************************************
    * Method       :    createClientTerms
    * Description  :    create a list of client terms
    * Parameter    :    NIL    
    * Return Type  :    list<TAFS_Client_Terms__c>
    *************************************************************************************/
    public static list<TAFS_Client_Terms__c> createClientTerms(integer total) 
    {
        list<TAFS_Client_Terms__c> clientTerms = new list<TAFS_Client_Terms__c>();
        for(integer i = 0; i < total; i++)
        {
            clientTerms.add(new TAFS_Client_Terms__c(Name = 'Test'+i));
                                                                
        }
        insert clientTerms;
        return clientTerms;
    }
    /************************************************************************************
    * Method       :    createAccounts
    * Description  :    create a list of accounts
    * Parameter    :    NIL    
    * Return Type  :    list<Account>
    *************************************************************************************/
    public static list<Account> createAccounts(integer total , Id clientTermId) 
    {
        list<Account> accounts = new list<Account>();
        
        for(integer i = 0; i < total; i++)
        {
            String uniqueness = DateTime.now()+':'+Math.random();
            try{ 
                throw new NullPointerException();
            }catch(Exception e){
                uniqueness += e.getStackTraceString(); //includes the top level test method name without having to pass it
            }
            String accountNumber = 'testaccountnumber-' + uniqueness.HashCode() + '-'+ i;
            accounts.add(new Account( 
                                    Name                                    = 'Test Account 001-' + uniqueness.HashCode(),
                                    AccountNumber                           =  accountNumber,
                                    TAFS_Client_Status__c                   = 'Client Created', 
                                    TAFS_Client_Credit_Check_Password__c    = 'test',
                                    AccountSource                           = 'test lead',                                    
                                    TAFS_Company_Type__c                    = 'Carrier',
                                    TAFS_Business_Type__c                   = 'test',
                                    TAFS_Currency_Type__c                   = 'USD',
                                    TAFS_UCC_Date__c                        =  Date.Today(),
                                    TAFS_Signed_Date__c                     =  Date.Today(),
                                    TAFS_Federal_Tax_ID__c                  = 'fed-001',
                                    TAFS_Default_Remittance_Method__c       = 'default',
                                    TAFS_Max_Invoice_Amount__c              =  200,
                                    Phone                                   = '9663066004',
                                    TAFS_Email__c                           = 'a@b.com',
                                    TAFS_Year_Started__c                    = 'test',
                                    TAFS_Office__c                          = 'US',
                                    TAFS_Client_Terms__c                    = clientTermId,
                                    BillingStreet                           = 'test',
                                    BillingState                            = 'test',
                                    BillingCity                             = 'test',
                                    BillingCountry                          = 'US',
                                    BillingPostalCode                       ='560100',
                                    TAFS_DOT__c                             = 'dot-001'
                                ));
        }
        
        insert accounts;
        return accounts;
    }
}